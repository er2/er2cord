# You can still use meson, but can make

NAME = er2cord
IPAT ?= /usr
TMP = ./.$(NAME)
#$(abspath ./.$(NAME))

all: build
build: build/$(NAME)

build/$(NAME):
	meson build --prefix="$(IPAT)"
	ninja -C build

deb: clean
	make DESTDIR="$(TMP)" install
	cp -r tools/DEBIAN $(TMP)
	chmod 755 $(TMP)/DEBIAN/postinst
	@dpkg-deb -b $(TMP) er2cord.deb
	rm -rf $(TMP)

install: build
	DESTDIR="../$(DESTDIR)" ninja -C build install
	find "$(DESTDIR)" -exec chmod 755 {} \;

uninstall: build
	ninja -C build uninstall

docs: build
	mkdir -p "$(IPAT)"
	find dist/ ! -name '*er2cord*' -exec cp -f "{}" "$(IPAT)" \;

clean:
	rm -rf build $(TMP)


/** Startup file
 * (c) Er2 2021
 * Zlib License
 */

static int main (string[] args) {
  var app = new Er2Cord.Launcher();
  return app.run(args);
}

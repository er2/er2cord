/** Main window
 * (c) Er2 2021
 * Zlib License
 */

using Gtk;
namespace Er2Cord {
  Chat chat;
  Scroller gld;
  Scroller chn;
  Scroller ppl;
  
  Entry text;

  class AppWindow : ApplicationWindow {
    construct {
      var css = new CssProvider();
      css.load_from_resource("/org/er2/cord/style.css");
      StyleContext.add_provider_for_display(
        get_style_context().display,
        css,
        STYLE_PROVIDER_PRIORITY_USER
      );

      set_default_size(500, 500);
      set_title(_("Er2Cord"));
      set_titlebar(win_title());

      chat = new Chat(new User());
      gld = new Scroller();
      chn = new Scroller(100);
      ppl = new Scroller();

      var main = new Grid();
      main.column_spacing = 2;
      
      main.attach(chat.scr, 2, 0);
      main.attach(new Avatar(chat.user.ava), 0, 1);
      main.attach(new Label(chat.user.name), 1, 1);
      
      text = new Entry();
      var send = new Button.with_label(_("Send"));
      text.show_emoji_icon = true;

      text.activate.connect(send_message);
      send.clicked.connect(send_message);
      
      main.attach(text, 2, 1);
      main.attach(send, 3, 1);
      
      main.attach(gld.scr, 0, 0);
      main.attach(chn.scr, 1, 0);
      main.attach(ppl.scr, 3, 0);

      chat.send_message("hello");

      child = main;
    }

    public Widget win_title() {
      var bar = new HeaderBar();

      var abtn = new MenuButton();
      var mbtn = new MenuButton();

      var aenu = new GLib.Menu();
      var menu = new GLib.Menu();
      var mfs = new GLib.Menu();
      var mus = new GLib.Menu();

      mfs.append(_("Me"), "app.me");
      mfs.append(_("Settings"), "app.settings");
      mus.append(_("Switch account"), "app.switch");
      mus.append(_("Log off"), "app.logoff");
      mus.append(_("Quit"), "app.quit");
      menu.append_section(null, mfs);
      menu.append_section(null, mus);
      
      aenu.append(_("Server"), "app.serv");
      aenu.append(_("Account"), "app.login");
      aenu.append(_("Friend"), "app.friend");
      aenu.append(_("Invite"), "app.invite");

      abtn.label = _("New");
      abtn.menu_model = aenu;
      mbtn.menu_model = menu;

      bar.pack_start(abtn);
      bar.pack_end(new Separator(VERTICAL));
      bar.pack_end(mbtn);
      return bar;
    }
    
    public void send_message() {
      chat.send_message(text.text);
      text.text = "";
    }

    public AppWindow(Gtk.Application app) { Object(application: app); }
  }
}

/** New server window
 * (c) Er2 2021
 * Zlib License
 */

using Gtk;
namespace Er2Cord {
  class NewServer : Window {
    Stack stack;

    construct {
      stack = new Stack();
      stack.vexpand = stack.hexpand = true;
      stack.transition_type = SLIDE_LEFT_RIGHT;
      pages();

      var main = new Box(VERTICAL, 2);
      main.append(ava_info());
      main.append(stack);
      main.append(new Button.with_label(_("Enter")));
      child = main;

      set_titlebar(win_title());
    }

    public Widget win_title() {
      var bar = new HeaderBar();
      var swi = new StackSwitcher();
      swi.stack = stack;
      bar.title_widget = swi;
      return bar;
    }

    public Box ava_info() {
      var box = new Box(HORIZONTAL, 2);
      box.halign = CENTER;
      box.append(new Image.from_icon_name("avatar-default-symbolic"));
      box.append(new Label("MyName at Discord"));
      return box;
    }
    
    private void pages() {
      var jpg = new Box(VERTICAL, 0);
      var jlk = new Entry();
      jlk.placeholder_text = _("Invite link");
      jpg.append(jlk);
      jpg.append(new Label(_("Examples:\n") +
"""https://discord.gg/discord
discord.gg/discord
discord
"""));

      var cpg = new Grid();
      var csn = new Entry();
      var crg = new ComboBox();
      cpg.row_homogeneous = cpg.column_homogeneous = true;
      csn.placeholder_text = _("Server name");
      cpg.attach(csn, 0, 0, 2, 1);
      cpg.attach(crg, 1, 1);
      cpg.attach(new Label(_("Server region")), 0, 1);
      cpg.attach(new Label("TODO: Avatars"), 2, 0, 1, 2);
      cpg.attach(new Label(_("By creating a server, you agree to Discord rules")), 0, 2, 2, 1);

      stack.add_titled(jpg, "1", _("Join"));
      stack.add_titled(cpg, "2", _("Create"));
    }
  }
}

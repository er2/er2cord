/** Settings window
 * (c) Er2 2021
 * Zlib License
 */

using Gtk;
namespace Er2Cord {
  class Settings : Window {
    public Config config;

    construct {
      set_title(_("Settings"));
      set_titlebar(win_title());

      var main = new Grid();
      main.column_spacing = 2;
      
      var ent = new Entry();

      child = main;
    }

    public Widget win_title() {
      var bar = new HeaderBar();
      return bar;
    }
  }
}

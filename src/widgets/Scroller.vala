/** Scroller class
 * (c) Er2 2021
 * Zlib License
 */

using Gtk;
namespace Er2Cord {
  class Scroller : Object {
    Box box;
    public ScrolledWindow scr; // FIXME: GTK Developers is pidors

    public Scroller(
      int w = 48,
      Box wid = new Box(VERTICAL, 0),
      bool exp = false
    ) {
      scr = new ScrolledWindow();
      if(w != 0) scr.width_request = w;

      scr.hscrollbar_policy = NEVER;
      scr.vexpand = true;
      scr.hexpand = exp;

      box = wid;
      scr.child = box;
    }

    public void append(Widget w) {
      box.append(w);
    }
  }
}

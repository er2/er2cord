/** Message widget
 * (c) Er2 2021
 * Zlib License
 */

using Gtk;
namespace Er2Cord {
  class MessageWid : Grid {
    public Message msg;

    public MessageWid(Message msg) {
      this.msg = msg;
      column_spacing = 4;

      var rbox = new Box(HORIZONTAL, 4);
      if(msg.reply != null) msgReply(rbox);
      attach(rbox, 0, 0, 3, 1);
      msgHead();
      msgContent();
    }

    public MessageWid.content(
      string text,
      User user,
      string date = "unk.",
      Message? reply = null,
      MessageEmbed[]? embeds = null
    ) {
      this(new Message(text, user, date, reply, embeds));
    }

    private void msgReply(Box box) {
      var nik = new Label(@"<b>$(msg.reply.user.name)</b>");
      var txt = new Label(string.joinv("\n", msg.reply.text.split("\n", 3)[0:2]));

      nik.use_markup = true;

      box.append(new Image.from_icon_name("mail-replied-symbolic"));
      box.append(nik);
      box.append(txt);
    }
    public void msgHead() {
      var nick = new Label(@"<b>$(msg.user.name)</b>");
      var date = new Label(msg.date);
      nick.use_markup = true;
      attach(new Avatar(msg.user.ava), 0, 1);
      attach(nick, 1, 1);
      attach(date, 2, 1);
    }
    
    public void msgContent() {
      var box = new Box(VERTICAL, 0);
      var txt = new Label(msg.text);

      txt.halign       = START;
      txt.wrap         = true;
      txt.wrap_mode    = CHAR;

      box.append(txt);
      attach(box, 1, 2, 2, 1);
    }
  }
}

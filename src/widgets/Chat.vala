/** Chat class
 * (c) Er2 2021
 * Zlib License
 */

using Gtk;
namespace Er2Cord {
  class Chat : Scroller {
    public User user;
    public string channel;
    Message[] msgs;

    public Chat(User user, string channel = "#channelName") {
      base(0, new Box(VERTICAL, 0), true);
      this.user = user;
      this.channel = channel;
      //this.msgs = new Message[1];

      var ava = new Avatar.gen(channel, User.getColors(1), 64);
      var big = new Gtk.Label("<span size='xx-large' weight='bold'>" + _("Welcome to %s!").printf(channel) + "</span>");
      var txt = new Gtk.Label(_("This is start of the %s.").printf(channel));
      var sep = new Gtk.Separator(HORIZONTAL);

      big.use_markup = true;
      big.halign  = START;
      txt.halign  = START;
      
      sep.margin_start  = 8;
      sep.margin_end    = 8;
      sep.margin_top    = 8;
      sep.margin_bottom = 8;

      append(ava);
      append(big);
      append(txt);
      append(sep);
    }

    public bool send_message(string text, Message? reply = null) {
      if(text == "") return false;
      var msg = new Message(text, user, "\\(*.*)/");
      this.msgs += msg;
      append(new MessageWid(msg));

      // https://stackoverflow.com/a/4479178
      var adj = scr.vadjustment;
      adj.value = adj.upper;
      scr.vadjustment = adj;
      return true;
    }
  }
}

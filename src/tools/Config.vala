/** Configuration using GSettings
 * (c) Er2 2021
 * Zlib License
 */

namespace Er2Cord {
  class Config : GLib.Settings {
    public string name { get; set; }
    
    public Config() {
      Object(schema_id: ID);
      
      //    var,   class, option  flags
      bind("name", this, "name", DEFAULT);
    }
  }
}

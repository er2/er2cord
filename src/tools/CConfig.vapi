[CCode (cprefix = "", lower_case_cprefix = "", cheader_filename = "config.h")]
namespace Er2Cord {
  public const string ID;
  public const string VERSION;
  public const string GETTEXT_PACKAGE;
  public const string DATADIR;
  public const string GNOMELOCALEDIR;
}

/** Message class
 * (c) Er2 2021
 * Zlib License
 */

namespace Er2Cord {
  class Message {
    public User     user;
    public string   text;
    public string   date;
    public Message? reply;
    public MessageEmbed[]? embeds;
    public Message(
      string text,
      User user,
      string date = "unk.",
      Message? reply = null,
      MessageEmbed[]? embeds = null
    ) {
      this.user = user;
      this.text = text;
      this.date = date;
      this.reply = reply;
      this.embeds = embeds;
    }
  }

  enum EmbedType {
    RICH,
    IMAGE,
    VIDEO,
    GIF,
    ARTICLE,
    LINK,
  }
  struct EmbedField {
    string name;
    string value;
    bool _inline;
  }
  struct MessageEmbed {
    EmbedType type;
    string? title;
    string? description;
    string? url;
    // string? timestamp;
    int? color;
    //? footer;
    //? image;
    //? thumbnail;
    //? video;
    //? provider;
    //? author;
    EmbedField[]? fields;
  }
}

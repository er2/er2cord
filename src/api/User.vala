/** User class
 * (c) Er2 2021
 * Zlib License
 */

namespace Er2Cord {
  class User {
    public Cairo.ImageSurface ava;
    public string name;
    public int discr;

    public static double[] getColors(int k) {
      switch(k) {
        case 0:  return {0.447, 0.537, 0.855};
        case 1:  return {0.459, 0.494, 0.541};
        case 2:  return {0.231, 0.647, 0.360};
        case 3:  return {0.980, 0.651, 0.102};
        case 4:  return {0.929, 0.259, 0.271};
        case 5:  return {0.922, 0.271, 0.624};
        default: return {0.000, 0.000, 0.000};
      }
    }

    public double[] color {
      owned get { return getColors(discr % 5); }
    }

    public User(
      string name = _("Clyde"),
      int discr   = 0000,
      Avatar? ava = null
    ) {
      this.name  = name;
      this.discr = discr;

      if(ava == null)
        this.ava = new Avatar.gen(name, color).ava;
      else this.ava = ava.ava;
    }
  }
}

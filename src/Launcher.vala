/** Launcher of window
 * (c) Er2 2021
 * Zlib License
 */

namespace Er2Cord {
  class Launcher : Gtk.Application {
    public Config config;

    public Launcher() {
      Object(
        application_id: ID,
        flags: GLib.ApplicationFlags.FLAGS_NONE
      );
      actions();
      config = new Config();
	  }
    public override void activate() {
      var win = this.active_window;
      if(win == null) win = new AppWindow(this);
      win.present();
    }
    
    public void actions() {
      // App-only actions!
      add_action_entries({
        {"quit", () => quit()
          , null, null, null},
        {"settings", () => {
          var wid = new Settings();
          wid.config = this.config;
          to_top(wid);
        }, null, null, null},
        {"serv", () => to_top(new NewServer())
          , null, null, null},
      }, this);
      set_accels_for_action("app.quit", {"<Control>q"});
      set_accels_for_action("app.settings", {"<Control>s"});
    }

    public void to_top(Gtk.Window wid) {
      wid.transient_for = active_window;
      wid.modal = true;
      wid.application = this;
      wid.present();
    }
  }
}
